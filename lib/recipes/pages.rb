# frozen_string_literal: true

# Defines a module named Recipes
module Recipes
  # Defines a new class called Pages, which inherits from the Recipes::Client
  # class
  class Pages < Client
    def self.generate!
      # Run the function generate_normal_page
      generate_normal_page('index', 'Recipes', 'All recipes', recipes)
      # Run the function generate_contributing_page
      generate_contributing_page('contribute', 'Contributions', 'Information on contributing')
      # Run the function generate_category_pages
      generate_category_pages
      # Loop over the items in the variable recipes
      recipes.each do |r|
        generate_recipe_page(r)
      end
    end

    def self.generate_category_pages
      # Loop over an Array derived from taking the value of the key category,
      # forced to lowercase, from the items within the variable recipes
      recipes.map { |r| r['category'].downcase }.each do |c|
        # Run the function generate_normal_page
        generate_normal_page(
          c.downcase.gsub(' ', '_'),
          c.capitalize.pluralize,
          "Recipes for #{c.capitalize.pluralize}",
          category_recipes(c)
        )
      end
    end

    def self.category_recipes(category)
      # Return an Array sorted by the key name derived from the items within the
      # variable recipes that have a value for the key category, forced to
      # lowercase equal to the variable category
      recipes.select { |r| r['category'].downcase == category }.sort_by { |r| r['name'] }
    end

    def self.generate_normal_page(page, title, description, list)
      # Set the variable output to an Array
      output = [
        header(title, description),
        navbar,
        normal_page(list),
        footer
      ]
      # Write the content of the variable output, converting into a String by
      # combining all items with a separate of a newline, to a file within the
      # public folder derived from the variable page
      File.write("public/#{page}.html", output.join("\n"))
    end

    def self.generate_contributing_page(page, title, description)
      # Set the variable output to an Array
      output = [
        header(title, description),
        navbar,
        contributing_page,
        footer
      ]
      # Write the content of the variable output, converting into a String by
      # combining all items with a separate of a newline, to a file within the
      # public folder derived from the variable page
      File.write("public/#{page}.html", output.join("\n"))
    end

    def self.generate_recipe_page(recipe)
      # Set the variable output to an Array
      output = [
        header(recipe['name'], recipe['description']),
        navbar,
        recipe_page(recipe),
        footer
      ]
      # Write the content of the variable output, converting into a String by
      # combining all items with a separate of a newline, to a file within the
      # public folder derived from the variable page
      File.write("public/#{recipe['name'].downcase.gsub(' ', '_')}.html", output.join("\n"))
      # tbd
    end

    def self.header(title, description)
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('header').result_with_hash(
        title: title, # rubocop:disable Style/HashSyntax
        description: description # rubocop:disable Style/HashSyntax
      )
    end

    def self.navbar
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('navbar').result_with_hash(
        recipes: recipes # rubocop:disable Style/HashSyntax
      )
    end

    def self.normal_page(content)
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('normal_page').result_with_hash(
        recipes: content
      )
    end

    def self.contributing_page
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('contribute').result_with_hash({})
    end

    def self.recipe_page(content)
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('recipe_page').result_with_hash(
        recipe: content
      )
    end

    def self.footer
      # Return the value of running the function result_with_hash on the value
      # returned by running the funciton renderer
      renderer('footer').result_with_hash({})
    end
  end
end
